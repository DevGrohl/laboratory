<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detail
 *
 * @ORM\Table(name="details")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DetailsRepository")
 */
class Detail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="details", cascade={"all"})
     * @ORM\JoinColumn(name="`order`", referencedColumnName="id")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="Exam", inversedBy="details")
     * @ORM\JoinColumn(name="exam", referencedColumnName="id")
     */
    private $exam;

    public function __toString()
    {
        return (string)$this->exam;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return Detail
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set exam
     *
     * @param Exam $exam
     *
     * @return Detail
     */
    public function setExam(Exam $exam = null)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam
     *
     * @return Exam
     */
    public function getExam()
    {
        return $this->exam;
    }
}
