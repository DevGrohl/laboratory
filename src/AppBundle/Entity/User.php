<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="user")
     */
    private $user_orders;

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="doctor", fetch="EXTRA_LAZY")
     */
    private $doctor_orders;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true, nullable=true)
     */
    private $username;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified_at", type="datetime")
     */
    private $modifiedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="created_by", type="integer")
     */
    private $createdBy = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="modified_by", type="integer")
     */
    private $modifiedBy = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false, options={"default":0})
     */
    private $type = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="branch_office", type="integer", nullable=false, options={"default":0})
     */
    private $branch_office;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="SharedFile", mappedBy="sender")
     */
    private $files_sended;

    /**
     * @ORM\OneToMany(targetEntity="SharedFile", mappedBy="recipient")
     */
    private $files_received;

    /**
     * User constructor.
     */
    public function __construct() {
        $this->isActive = true;
        $this->user_orders = new ArrayCollection();
        $this->doctor_orders = new ArrayCollection();
        $this->files_received = new ArrayCollection();
        $this->files_sended = new ArrayCollection();
    }

    /**
     * User toString method
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return User
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return User
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     *
     * @return User
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return int
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set branch_office
     *
     * @param integer $branch_office
     *
     * @return User
     */
    public function setBranchOffice($branch_office)
    {
        $this->branch_office = $branch_office;

        return $this;
    }

    /**
     * Get branch_office
     *
     * @return int
     */
    public function getBranchOffice()
    {
        return $this->branch_office;
    }

    /**
     * Add order
     *
     * @param Order $user_orders
     *
     * @return User
     */
    public function addUserOrder(Order $user_orders)
    {
        $this->user_orders[] = $user_orders;

        return $this;
    }

    /**
     * Remove order
     *
     * @param Order $user_orders
     */
    public function removeUserOrder(Order $user_orders)
    {
        $this->user_orders->removeElement($user_orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserOrders()
    {
        return $this->user_orders;
    }

    /**
     * String representation of object
     *
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password
        ));
    }

    /**
     * Constructs the object
     *
     * @param string $serialized
     * @return void
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password
        ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return array
     */
    public function getRoles()
    {
        if ($this->type == 0){
            return array('ROLE_ADM');
        } elseif ($this->type == 1){
            return array('ROLE_LAB');
        } elseif ($this->type == 2){
            return array('ROLE_DOC');
        } else{
            return array('ROLE_REC');
        }
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add doctorOrder
     *
     * @param Order $doctorOrder
     *
     * @return User
     */
    public function addDoctorOrder(Order $doctorOrder)
    {
        $this->doctor_orders[] = $doctorOrder;

        return $this;
    }

    /**
     * Remove doctorOrder
     *
     * @param Order $doctorOrder
     */
    public function removeDoctorOrder(Order $doctorOrder)
    {
        $this->doctor_orders->removeElement($doctorOrder);
    }

    /**
     * Get doctorOrders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctorOrders()
    {
        return $this->doctor_orders;
    }

    /**
     * Add filesSended
     *
     * @param SharedFile $filesSended
     *
     * @return User
     */
    public function addFilesSended(SharedFile $filesSended)
    {
        $this->files_sended[] = $filesSended;

        return $this;
    }

    /**
     * Remove filesSended
     *
     * @param SharedFile $filesSended
     */
    public function removeFilesSended(SharedFile $filesSended)
    {
        $this->files_sended->removeElement($filesSended);
    }

    /**
     * Get filesSended
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilesSended()
    {
        return $this->files_sended;
    }

    /**
     * Add filesReceived
     *
     * @param SharedFile $filesReceived
     *
     * @return User
     */
    public function addFilesReceived(SharedFile $filesReceived)
    {
        $this->files_received[] = $filesReceived;

        return $this;
    }

    /**
     * Remove filesReceived
     *
     * @param SharedFile $filesReceived
     */
    public function removeFilesReceived(SharedFile $filesReceived)
    {
        $this->files_received->removeElement($filesReceived);
    }

    /**
     * Get filesReceived
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilesReceived()
    {
        return $this->files_received;
    }
}
