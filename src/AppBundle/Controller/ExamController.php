<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Exam;
use AppBundle\Entity\Field;
use AppBundle\Entity\Result;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Exam controller.
 *
 * @Route("exam")
 */
class ExamController extends Controller
{
    /**
     * Lists all exam entities.
     *
     * @Route("/", name="exam_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $branch = $this->getUser()->getBranchOffice();

        $repo = $em->getRepository('AppBundle:Exam');
        $exams = ($branch !== 0) ? $repo->findAllExamsFromSameBranch($branch) : $repo->findBy(array(), array('id' => 'DESC'));

        return $this->render('exam/index.html.twig', array(
            'exams' => $exams,
        ));
    }

    /**
     * Creates a new exam entity.
     *
     * @Route("/new", name="exam_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $exam = new Exam();
        $form = $this->createForm('AppBundle\Form\ExamType', $exam, ['user' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $exam->setCreatedBy($this->getUser()->getId());
            $exam->setModifiedBy($this->getUser()->getId());
            $em->persist($exam);
            $em->flush();

            return $this->redirectToRoute('exam_show', array('id' => $exam->getId()));
        }

        return $this->render('exam/new.html.twig', array(
            'exam' => $exam,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a exam entity.
     *
     * @Route("/{id}", name="exam_show")
     * @Method("GET")
     * @param Exam $exam
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Exam $exam)
    {
        $deleteForm = $this->createDeleteForm($exam);

        return $this->render('exam/show.html.twig', array(
            'exam' => $exam,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing exam entity.
     *
     * @Route("/{id}/edit", name="exam_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Exam $exam
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Exam $exam)
    {
        $deleteForm = $this->createDeleteForm($exam);
        $editForm = $this->createForm('AppBundle\Form\ExamType', $exam, ['user' => $this->getUser()]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('exam_edit', array('id' => $exam->getId()));
        }

        return $this->render('exam/edit.html.twig', array(
            'exam' => $exam,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a exam entity.
     *
     * @Route("/{id}", name="exam_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Exam $exam
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Exam $exam)
    {
        $form = $this->createDeleteForm($exam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($exam);
            $em->flush();
        }

        return $this->redirectToRoute('exam_index');
    }

    /**
     * @Route("/save_field_modal", name="save_field_modal")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function saveDoctorAction(Request $request)
    {
        $result = [];
        if (!is_null($request->request->get('n'))) {
            $em = $this->getDoctrine()->getManager();
            $field = new Field();
            $field->setName($request->request->get('n'));
            $field->setReference($request->request->get('r'));
            $field->setUnit($request->request->get('u'));
            $field->setIsTitle($request->request->get('t'));
            $field->setCreatedBy($this->getUser());
            $field->setModifiedBy($this->getUser());
            $field->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            $field->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));
            $em->persist($field);
            $em->flush();
            $result = ['id' => $field->getId()];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/save_results", name="save_results")
     * @param Request $request
     * @return JsonResponse
     */
    public function saveResultsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository("AppBundle:Order")->find($request->request->get('order'));

        $results = $em->getRepository("AppBundle:Result")->findBy(["order"=>$order->getId()]);
        foreach($results as $r){
            $em->remove($r);
        }
        $em->flush();

        foreach($request->request as $f => $v) {
            if($f !== "order") {
                $field = $em->getRepository("AppBundle:Field")->find($f);
                $result = new Result();
                $result->setOrder($order);
                $result->setField($field);
                $result->setValue($v);
                $em->persist($result);
            }
        }
        $em->flush();
        $result = ['value' => true];

        return new JsonResponse($result);
    }

    /**
     * Creates a form to delete a exam entity.
     *
     * @param Exam $exam The exam entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Exam $exam)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('exam_delete', array('id' => $exam->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
