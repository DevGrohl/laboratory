<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ReportController extends Controller
{
    /**
     * @Route("/reports", name="reports_index")
     */
    public function IndexReportAction()
    {
        return $this->render('AppBundle:Report:index_report.html.twig');
    }

    /**
     * @Route("/reports/daily_report", name="reports_daily")
     */
    public function DailyReportAction()
    {

        $em = $this->getDoctrine()->getManager();
        $branch = $this->getUser()->getBranchOffice();

        $repo = $em->getRepository('AppBundle:Order');
        $orders = ($branch !== 0) ? $repo->findAllOrdersFromSameBranch($branch) : $repo->findBy(array(), array('id' => 'DESC'), 18000);

        $dates  = array();
        foreach ($orders as $order){
            $dates[] = $order->getCreatedAt()->format('Y-m-d');
        }

        $result = array_count_values($dates);


        return $this->render('AppBundle:Report:daily_report.html.twig', array(
            'result' => $result
        ));
    }

    /**
     * @Route("/reports/daily_report_date/{date}", name="reports_daily_date")
     * @param $date
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function DailyReportDateAction($date)
    {
        $em = $this->getDoctrine()->getRepository('AppBundle:Order');
        $qb = $em->createQueryBuilder('o');
        $branch = $this->getUser()->getBranchOffice();

        if ($branch !== 0){
           $qb->innerJoin('o.user', 'u', 'WITH', 'u.branch_office = :branch');
           $qb->andWhere('u.id = o.user');
           $qb->setParameter('branch', $branch);
        }

        $qb->where('YEAR(o.createdAt) = :year')
            ->andWhere('MONTH(o.createdAt) = :month')
            ->andWhere('DAY(o.createdAt) = :day');

        $date_format = new \DateTime($date);

        $qb->setParameter('year', $date_format->format('Y'))
            ->setParameter('month', $date_format->format('m'))
            ->setParameter('day', $date_format->format('d'));

        $result = $qb->getQuery()->execute();

        return $this->render('AppBundle:Report:daily_report_date.html.twig', array(
            'date' => $date,
            'result' => $result
        ));
    }

    /**
     * @Route("/reports/commission_report", name="reports_commission")
     */
    public function CommissionReportAction()
    {
        $em = $this->getDoctrine()->getRepository('AppBundle:User');
        $branch = $this->getUser()->getBranchOffice();
        if ($branch !== 0) {
            $doctors = $em->findBy(array('type' => 2, 'branch_office' => $branch), array('name' => 'ASC'));
        } else {
            $doctors = $em->findBy(array('type' => 2), array('name' => 'ASC'));
        }

        $doctorsName = array ();
        foreach ($doctors as $value) {
            $id = $value->getId();
            $name = $value->getName();
            array_push($doctorsName, array("id"=>$id,"name"=>$name));
        }
        return $this->render('AppBundle:Report:commission_report.html.twig', array(
            'doctors' => $doctorsName
        ));
    }

    /**
     * @Route("/reports/commission_report_doctor/{user}", name="reports_commission_doctor")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function CommissionReportDoctorAction(User $user)
    {
        $em = $this->getDoctrine()->getRepository('AppBundle:Order');
        
        
        $orders = $em->findBy(array('doctor' => $user), array('id' => 'DESC'), 1000);
        
        return $this->render('AppBundle:Report:commission_report_doctor.html.twig', array(
            'orders' => $orders, 'user' => $user->getName()
        ));
    }

    /**
     * @Route("/reports/general_report", name="reports_general")
     */
    public function GeneralReportAction()
    {
        return $this->render('AppBundle:Report:general_report.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/reports/newer_report", name="newer_report")
     */
    public function NewerReportAction()
    {
        $date = date('Y-m-d');

        return $this->redirectToRoute('reports_daily_date', array('date' => $date));
    }

}
