<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
// use Gremo\HighchartsBundle\GremoHighchartsBundle;

/**
 * Graphic controller.
 *
 * @Route("graphic")
 */
class GraphicController extends Controller
{
    /**
     * Lists all graphic entities.
     *
     * @Route("/", name="graphic_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        // /** @var $highcharts \Gremo\HighchartsBundle\Highcharts */
        // $highcharts = $this->get('gremo_highcharts');

        // $highcharts->newAreaChart();

        // $chart = $highcharts->newLineChart()
        //    ->newChart()
        //        ->setRenderTo('chart-container')
        //    ->getParent()
        //    ->newTitle()
        //        ->setText('My chart')
        //        ->newStyle()
        //            ->setColor('#FF00FF')
        //            ->setFontWeight('bold')
        //        ->getParent()
        //    ->getParent();


        // return $this->render('AppBundle:Graphic:index.html.twig', array('chart' => $chart));
        return $this->render('AppBundle:Graphic:index.html.twig');
    }

    /**
     * @Route("/get_exam_list/{valores}", name="get_exam_list")
     * @param $valores
     * @return JsonResponse
     */
    public function getExamListAction($valores)
    {

        var_dump($valores);

        $valores = explode(",", $valores);

            var_dump($valores[0]);
        $name = $valores[0];

        $em = $this->getDoctrine()->getManager();
        // // $em = $this->getDoctrine()->getRepository('AppBundle:Order');
        // $qb = $em->createQueryBuilder('o');

        // $objects = $em->getRepository('AppBundle:Order')->createQueryBuilder('q')
        //     ->innerJoin('q.patient', 'p')
            
        //     ->where('p.name LIKE :patient')
        //     ->setParameter('patient', '%MARIA GUADALUPE HERNANDEZ%')
        //     ->setMaxResults(20)
        //     ->getQuery()->getResult();
// $rsm = new ResultSetMapping();
// $query = $em->createNativeQuery('SELECT o.* FROM `orders` o
// INNER JOIN patients ON o.patient = patients.id 
// INNER JOIN details ON details.order = o.id 
// INNER JOIN exams ON exams.id = details.exam
// WHERE patients.name LIKE ? AND exams.type = 1', $rsm);
// $query->setParameter(1, "%MARIA%");
// // $query->setParameter(2, 1);
// $users = $query->getResult();

        $connection = $em->getConnection();
$statement = $connection->prepare("SELECT * FROM `orders` INNER JOIN patients ON orders.patient = patients.id INNER JOIN details ON details.order = orders.id INNER JOIN exams ON exams.id = details.exam WHERE patients.name LIKE :name AND exams.type = 1 AND exams.category != 1");
$statement->bindValue('name', $name);
$statement->execute();
$results = $statement->fetchAll();
var_dump($results);

        // // $objects = $em->getRepository('AppBundle:Order')->createQueryBuilder('q')
        // //     ->innerJoin('q.patient', 'p')
        // //     ->where('p.name LIKE :patient')
        // //     ->orWhere('q.createdAt LIKE :date')
        // //     ->orderBy('q.id', 'DESC')
        // //     ->setFirstResult($start)
        // //     ->setMaxResults($length)
        // //     ->setParameter('patient', '%'.$search['value'].'%')
        // //     ->setParameter('date', '%'.$search['value'].'%')
        // //     ->getQuery()->getResult();

        // //$result = [$valores];
        // // if ($discount) {
        // //     $result = ['value' => $discount->getDiscount()];
        // // }
        // $result = ['status' => 0];

        return new JsonResponse($results);
    }



}
