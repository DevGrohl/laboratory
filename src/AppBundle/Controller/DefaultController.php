<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     *
     * @Route("/", name="default_index")
     */
    public function indexAction()
    {
        $user = $this->getUser();

        return $this->render('default/index.html.twig', array(
            'user' => $user ));
    }

    /**
     * @Route("/translation/", name="dataTable_translation")
     * @return JsonResponse
     */
    public function translateDatatable(): JsonResponse
    {
        $translation = array (
            'sProcessing' => 'Procesando...',
            'sLengthMenu' => 'Mostrar _MENU_ registros',
            'sZeroRecords' => 'No se encontraron resultados',
            'sEmptyTable' => 'Ningún dato disponible en esta tabla',
            'sInfo' => 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
            'sInfoEmpty' => 'Mostrando registros del 0 al 0 de un total de 0 registros',
            'sInfoFiltered' => '(filtrado de un total de _MAX_ registros)',
            'sInfoPostFix' => '',
            'sSearch' => 'Buscar:',
            'sUrl' => '',
            'sInfoThousands' => ',',
            'sLoadingRecords' => 'Cargando...',
            'oPaginate' =>
                array (
                    'sFirst' => 'Primero',
                    'sLast' => 'Último',
                    'sNext' => 'Siguiente',
                    'sPrevious' => 'Anterior',
                ),
            'oAria' =>
                array (
                    'sSortAscending' => ': Activar para ordenar la columna de manera ascendente',
                    'sSortDescending' => ': Activar para ordenar la columna de manera descendente',
                ),
        );
        return new JsonResponse($translation);
    }
}
