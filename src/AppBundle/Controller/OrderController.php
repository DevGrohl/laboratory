<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\Discount;
use AppBundle\Entity\Order;
use AppBundle\Entity\Detail;
use AppBundle\Entity\ResultFile;
use AppBundle\Entity\User;
use AppBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Order controller.
 *
 * @Route("order")
 */
class OrderController extends Controller
{
    /**
     * Lists all order entities.
     *
     * @Route("/", name="order_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('order/index.html.twig');
    }

    /**
     * Lists all order entities.
     *
     * @Route("/doctor/{id}", name="order_index_doctor")
     * @Method("GET")
     * @param User $doctor
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexDoctorAction(User $doctor)
    {
        $em = $this->getDoctrine()->getManager();

        $orders = $em->getRepository('AppBundle:Order')->findBy(array('doctor' => $doctor->getId()), array('id' => 'DESC'));

        return $this->render('order/index.html.twig', array(
            'orders' => $orders,
        ));
    }

    /**
     * Creates a new order entity.
     *
     * @Route("/new", name="order_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $order = new Order();
        $form = $this->createForm('AppBundle\Form\OrderType', $order, ['user' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $exams = $form->get('laboratory_1')->getData()->toArray();
            $exams = array_merge($form->get('laboratory_2')->getData()->toArray(), $exams);
            $exams = array_merge($form->get('xray')->getData()->toArray(), $exams);
            $exams = array_merge($form->get('echo')->getData()->toArray(), $exams);
            $exams = array_merge($form->get('contrasted')->getData()->toArray(), $exams);
            $exams = array_merge($form->get('mama')->getData()->toArray(), $exams);
            $exams = array_merge($form->get('scan')->getData()->toArray(), $exams);

            $orderDate = new \DateTime($form->get('created_at')->getData());

            $order->setCreatedAt($orderDate);

            $order->setUser($this->getUser());


            foreach ($exams as $exam){
                $detail = new Detail();
                $detail->setOrder($order);
                $detail->setExam($exam);
                $order->addDetail($detail);

                $em->persist($detail);
                $em->persist($order);
            }

            $em->persist($order);
            $em->flush();

            $order = new Order();
            $form = $this->createForm('AppBundle\Form\OrderType', $order, ['user' => $this->getUser()]);
            return $this->render('order/new.html.twig', array(
                'order' => $order,
                'form' => $form->createView(),
            ));
        }

        return $this->render('order/new.html.twig', array(
            'order' => $order,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a order entity.
     *
     * @Route("/{id}", name="order_show")
     * @Method("GET")
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Order $order)
    {
        $deleteForm = $this->createDeleteForm($order);

        return $this->render('order/show.html.twig', array(
            'order' => $order,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a order entity.
     *
     * @Route("/detail/{id}", name="order_detail")
     * @Method({"GET", "POST"})
     * @param Order $order
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showDetailsAction(Order $order, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $resultFile = new ResultFile();
        $form = $this->createForm('AppBundle\Form\UploadType', $resultFile);
        $form->handleRequest($request);

        $results = $em->getRepository("AppBundle:Result")->findBy(["order" => $order->getId()]);
        $array = [];
        foreach ($results as $r) {
            $array[$r->getField()->getId()] = $r->getValue();
        }

        if ($form->isSubmitted() && $form->isValid()) {


            $fileSystem = new Filesystem();
            $fileSystem->exists($this->getParameter('results_dir'));
            if ($fileSystem->exists($this->getParameter('results_dir').'/'.$order->getId())) {
                $fileSystem->remove([$this->getParameter('results_dir').'/'.$order->getId()]);

                $deleteFile = $em->getRepository('AppBundle:ResultFile')->findOneBy(['order' => $order->getId()]);
                $em->remove($deleteFile);
                $em->flush();
            }

            $file = $resultFile->getFile();
            $fileUploader = new FileUploader($this->getParameter('results_dir').'/'.$order->getId());

            /** @var UploadedFile $file */
            $fileName = $fileUploader->upload($file);

            $resultFile->setFile($fileName);
            $resultFile->setOrder($order);
            $resultFile->setFilename($file->getClientOriginalName());

            $em->persist($resultFile);
            $em->flush();

            return $this->render('order/detail.html.twig', array(
                'resultFile' => $resultFile,
                'order' => $order,
                'results' => $array,
                'form' => $form->createView()
            ));
        }

        return $this->render('order/detail.html.twig', array(
            'resultFile' => $resultFile,
            'order' => $order,
            'results' => $array,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/download/{id}", name="download_file")
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadAction(Order $order)
    {
        $em = $this->getDoctrine()->getManager();
        $downloadFile = $em->getRepository('AppBundle:ResultFile')->findOneBy(['order' => $order->getId()]);
        $pdfPath = $this->getParameter('results_dir').'/'.$order->getId().'/'.$downloadFile->getFile();

        return $this->file($pdfPath, $downloadFile->getFilename());
    }

    /**
     * Displays a form to edit an existing order entity.
     *
     * @Route("/{id}/edit", name="order_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Order $order)
    {
        $deleteForm = $this->createDeleteForm($order);

        // var_dump($editForm->get('created_at')->getData()->format('Y-m-d'));
        // $dateC = $order->getCreatedAt()->format('Y-m-d');
        // var_dump($dateC);
        // $orderDate = new \DateTime(($editForm->get('created_at')->getData())->format('Y-m-d'));
        // $order->setCreatedAt($dateC);
        $editForm = $this->createForm('AppBundle\Form\EditOrderType', $order, ['user' => $this->getUser()]);
        $editForm->handleRequest($request);

         // var_dump($editForm->get('created_at')->getData());

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('order_edit', array('id' => $order->getId()));
        }
        return $this->render('order/edit.html.twig', array(
            'order' => $order,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a order entity.
     *
     * @Route("/{id}", name="order_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Order $order)
    {
        $form = $this->createDeleteForm($order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($order);
            $em->flush();
        }

        return $this->redirectToRoute('order_index');
    }

    /**
     * Creates a form to delete a order entity.
     *
     * @param Order $order The order entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Order $order)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('order_delete', array('id' => $order->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/detail/preview/{id}", name="order_preview")
     * @Method("GET")
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function previewAction(Order $order)
    {
        $data['patient'] = $order->getPatient()->getName();
        $data['doctor'] = $order->getDoctor()->getName();

        foreach($order->getDetails() as $detail){
            foreach($order->getResults() as $result){
                if (in_array($result->getField(), $detail->getExam()->getFields()->toArray())){
                    $data['exams'][$detail->getExam()->getTag()][] = $result;
                }
            }
        }

        return $this->render('order/preview.html.twig', array(
            'data' => $data
        ));
    }

    /**
     * @Route("/result/order_print/{id}", name="order_results")
     * @Method("GET")
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function DailyReportDateAction(Order $order)
    {
        $data['patient'] = $order->getPatient()->getName();
        $data['doctor'] = $order->getDoctor()->getName();
        $data['age'] = $order->getPatient()->getAge();

        foreach($order->getDetails() as $detail){
            foreach($order->getResults() as $result){
                if (in_array($result->getField(), $detail->getExam()->getFields()->toArray())){
                    $nombre = $detail->getExam()->getTag();
                    $id = $detail->getExam()->getId();
                    $data['exams'][$nombre.",".$id][] = $result;
                }
            }
        }

        return $this->render('AppBundle:Order:results.html.twig', array(
            'data' => $data
        ));
    }

    /**
     * @Route("/get_price/", name="get_price_exam")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function getPriceAction(Request $request)
    {
        $exam_id = $request->request->get('exam_id');
        if($exam_id) {
            $em = $this->getDoctrine()->getRepository('AppBundle:Exam');
            $exam = $em->findOneBy(array('id' => $exam_id));
            $result = [
                'price' => $exam->getCost()
            ];

            return new JsonResponse($result);
        }
        return new JsonResponse(['status' => 'Failed']);
    }

    /**
     * @Route("/get_discount_value/{id}", name="get_discount_value")
     * @param Discount $discount
     * @return JsonResponse
     */
    public function getDiscountValueAction(Discount $discount = null)
    {
        $result = ['value' => 0];
        if ($discount) {
            $result = ['value' => $discount->getDiscount()];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/get_company_value/{id}", name="get_company_value")
     * @param Company $company
     * @return JsonResponse
     */
    public function getCompanyValueAction(Company $company = null)
    {
        $result = ['value' => 0];
        if ($company) {
            $result = ['value' => $company->getDiscount()];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/save_doctor_modal/{name}", name="save_doctor_modal")
     * @param null $name
     * @return JsonResponse
     */
    public function saveDoctorAction($name = null)
    {
        $result = ['value' => 0];
        if ($name != "") {
            $em = $this->getDoctrine()->getManager();
            $user = new User();
            $user->setName($name);
            $user->setType(2);
            $em->persist($user);
            $em->flush();
            $result = ['id' => $user->getId()];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/delete_order/{id}", name="delete_order")
     * @param Order $order
     * @return JsonResponse
     */
    public function deleteOrderAction(Order $order = null)
    {
        $result = ['status' => 0];
        if ($order) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($order);
            $em->flush();
            $result = ['status' => 1];
        }
        return new JsonResponse($result);
    }

    /**
     * @Route("/ajax_orders", name="ajax_orders")
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function ajaxOrderAction(Request $request)
    {
//        $branch = $this->getUser()->getBranchOffice();

        // Get the parameters from DataTable Ajax Call
        if ($request->getMethod() == 'POST') {
            $draw = intval($request->request->get('draw'));
            $start = $request->request->get('start');
            $length = $request->request->get('length');
            $search = $request->request->get('search');
            $orders = $request->request->get('order');
            $columns = $request->request->get('columns');
        }
        else // If the request is not a POST one, die hard
            die;

        foreach ($orders as $key => $order) {
            // Orders does not contain the name of the column, but its number,
            // so add the name so we can handle it just like the $columns array
            $orders[$key]['name'] = $columns[$order['column']]['name'];
        }

        $em = $this->getDoctrine()->getManager();
//        $otherConditions = ['branch' => $branch];

//        $objects = $em->getRepository('AppBundle:Order')->findBy([], [$orders[0]['name'] => $orders[0]['dir']], $length, $start);

        $objects = $em->getRepository('AppBundle:Order')->createQueryBuilder('q')
            ->innerJoin('q.patient', 'p')
            ->where('p.name LIKE :patient')
            ->orWhere('q.createdAt LIKE :date')
            ->orderBy('q.id', 'DESC')
            ->setFirstResult($start)
            ->setMaxResults($length)
            ->setParameter('patient', '%'.$search['value'].'%')
            ->setParameter('date', '%'.$search['value'].'%')
            ->getQuery()->getResult();

        $qb = $em->createQueryBuilder('o');
        $qb->select('count (o.id)')->from('AppBundle:Order', 'o');
        $count = $qb->getQuery()->getSingleScalarResult();

        $filtered_objects_count = $em->getRepository('AppBundle:Order')->createQueryBuilder('cq')
            ->select('count(cq.id)')
            ->innerJoin('cq.patient', 'p')
            ->where('p.name LIKE :patient')
            ->orWhere('cq.createdAt LIKE :date')
            ->setParameter('patient', '%'.$search['value'].'%')
            ->setParameter('date', '%'.$search['value'].'%')
            ->getQuery()->getSingleScalarResult();

        $total_objects_count = $count;

        $response = [
            'draw' => $draw,
            'recordsTotal' => (int)$total_objects_count,
            'recordsFiltered' => (int)$filtered_objects_count,
            'data' => $this->entityToDatatables($objects)
        ];

        return new JsonResponse($response);
    }

    /**
     * @Route("/datatable/", name="datatable")
     * @param $objects
     * @return array
     */
    public function entityToDatatables($objects = null)
    {
        $result = [];
        foreach ($objects as $key => $object) {
            $result[] = [
                $object->getId(),
                $object->getPatient()->getName(),
                $object->getTotal(),
                $object->getRetainer(),
                $object->getStatus()->getName(),
                $object->getCreatedAt()->format('Y-m-d H:i:s'),
            ];
        }
        return $result;
    }
}

