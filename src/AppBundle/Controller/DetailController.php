<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Detail;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Detail controller.
 *
 * @Route("detail")
 */
class DetailController extends Controller
{
    /**
     * Lists all detail entities.
     *
     * @Route("/", name="detail_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $details = $em->getRepository('AppBundle:Detail')->findAll();

        return $this->render('detail/index.html.twig', array(
            'details' => $details,
        ));
    }

    /**
     * Creates a new detail entity.
     *
     * @Route("/new", name="detail_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $detail = new Detail();
        $form = $this->createForm('AppBundle\Form\DetailType', $detail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($detail);
            $em->flush();

            return $this->redirectToRoute('detail_show', array('id' => $detail->getId()));
        }

        return $this->render('detail/new.html.twig', array(
            'detail' => $detail,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a detail entity.
     *
     * @Route("/{id}", name="detail_show")
     * @Method("GET")
     * @param Detail $detail
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Detail $detail)
    {
        $deleteForm = $this->createDeleteForm($detail);

        return $this->render('detail/show.html.twig', array(
            'detail' => $detail,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing detail entity.
     *
     * @Route("/{id}/edit", name="detail_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Detail $detail
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Detail $detail)
    {
        $deleteForm = $this->createDeleteForm($detail);
        $editForm = $this->createForm('AppBundle\Form\DetailType', $detail);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('detail_edit', array('id' => $detail->getId()));
        }

        return $this->render('detail/edit.html.twig', array(
            'detail' => $detail,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a detail entity.
     *
     * @Route("/{id}", name="detail_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Detail $detail
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Detail $detail)
    {
        $form = $this->createDeleteForm($detail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($detail);
            $em->flush();
        }

        return $this->redirectToRoute('detail_index');
    }

    /**
     * Creates a form to delete a detail entity.
     *
     * @param Detail $detail The detail entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Detail $detail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('detail_delete', array('id' => $detail->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
