<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SharedFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\FileUploader;

/**
 * Sharedfile controller.
 *
 * @Route("sharedfile")
 */
class SharedFileController extends Controller
{
    /**
     * Lists all sharedFile entities.
     *
     * @Route("/", name="sharedfile_index")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sharedFiles = $em->getRepository('AppBundle:SharedFile')->findAll();
        $delete_forms = array();

        foreach($sharedFiles as $file){
            $delete_forms[$file->getId()] = $this->createDeleteForm($file)->createView();
        }

        return $this->render('sharedfile/index.html.twig', array(
            'sharedFiles' => $sharedFiles,
            'deleteForms' => $delete_forms
        ));
    }

    /**
     * Creates a new sharedFile entity.
     *
     * @Route("/new", name="sharedfile_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $sharedFile = new Sharedfile();
        $form = $this->createForm('AppBundle\Form\SharedFileType', $sharedFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $sharedFile->getFile();
            $fileUploader = new FileUploader($this->getParameter('sharedfiles_dir').'/'.$sharedFile->getRecipient()->getId());

            /** @var UploadedFile $file */
            $fileName = $fileUploader->upload($file);

            $sharedFile->setFile($fileName);
            $sharedFile->setSender($this->getUser());
            $sharedFile->setFilename($file->getClientOriginalName());

            $em = $this->getDoctrine()->getManager();
            $em->persist($sharedFile);
            $em->flush();

            return $this->redirectToRoute('sharedfile_index');
        }

        return $this->render('sharedfile/new.html.twig', array(
            'sharedFile' => $sharedFile,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sharedFile entity.
     *
     * @Route("/{id}", name="sharedfile_show")
     * @Method("GET")
     * @param SharedFile $sharedFile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(SharedFile $sharedFile)
    {
        $pdf = new File($this->getParameter('sharedfiles_dir').'/'.$sharedFile->getRecipient()->getId().'/'.$sharedFile->getFile());

        return $this->file($pdf);
    }

    /**
     * List the shared files for a doctor
     *
     * @Route ("/doctor/", name="sharedfile_list")
     * @Method("GET")
     */
    public function listAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getRepository('AppBundle:SharedFile');
        $files = $em->findBy(array(
            'recipient' => $user->getId()
        ));

        return $this->render('sharedfile/list.html.twig', array(
            'sharedFiles' => $files
        ));
    }

    /**
     * Displays a form to edit an existing sharedFile entity.
     *
     * @Route("/{id}/edit", name="sharedfile_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param SharedFile $sharedFile
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, SharedFile $sharedFile)
    {
        $sharedFile->setFile($this->getParameter('sharedfiles_dir').'/'.$sharedFile->getFile());
        $deleteForm = $this->createDeleteForm($sharedFile);
        $editForm = $this->createForm('AppBundle\Form\SharedFileType', $sharedFile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sharedfile_edit', array('id' => $sharedFile->getId()));
        }

        return $this->render('sharedfile/edit.html.twig', array(
            'sharedFile' => $sharedFile,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sharedFile entity.
     *
     * @Route("/{id}", name="sharedfile_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param SharedFile $sharedFile
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, SharedFile $sharedFile)
    {
        $form = $this->createDeleteForm($sharedFile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sharedFile);
            $em->flush();
        }

        return $this->redirectToRoute('sharedfile_index');
    }

    /**
     * Creates a form to delete a sharedFile entity.
     *
     * @param SharedFile $sharedFile The sharedFile entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(SharedFile $sharedFile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sharedfile_delete', array('id' => $sharedFile->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
