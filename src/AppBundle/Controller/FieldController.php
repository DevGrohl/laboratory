<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Field;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Field controller.
 *
 * @Route("field")
 */
class FieldController extends Controller
{
    /**
     * Lists all field entities.
     *
     * @Route("/", name="field_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $branch = $this->getUser()->getBranchOffice();

        $repo = $em->getRepository('AppBundle:Field');
        $fields = ($branch !== 0) ? $repo->findAllFieldsFromSameBranch($branch) : $repo->findBy(array(), array('id' => 'DESC'));

        return $this->render('field/index.html.twig', array(
            'fields' => $fields,
        ));
    }

    /**
     * Creates a new field entity.
     *
     * @Route("/new", name="field_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $field = new Field();
        $form = $this->createForm('AppBundle\Form\FieldType', $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $field->setCreatedBy($this->getUser()->getId());
            $field->setModifiedBy($this->getUser()->getId());
            $em->persist($field);
            $em->flush();

            return $this->redirectToRoute('field_show', array('id' => $field->getId()));
        }

        return $this->render('field/new.html.twig', array(
            'field' => $field,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a field entity.
     *
     * @Route("/{id}", name="field_show")
     * @Method("GET")
     * @param Field $field
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Field $field)
    {
        $deleteForm = $this->createDeleteForm($field);

        return $this->render('field/show.html.twig', array(
            'field' => $field,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing field entity.
     *
     * @Route("/{id}/edit", name="field_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Field $field
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Field $field)
    {
        $deleteForm = $this->createDeleteForm($field);
        $editForm = $this->createForm('AppBundle\Form\FieldType', $field);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('field_edit', array('id' => $field->getId()));
        }

        return $this->render('field/edit.html.twig', array(
            'field' => $field,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a field entity.
     *
     * @Route("/{id}", name="field_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Field $field
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Field $field)
    {
        $form = $this->createDeleteForm($field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($field);
            $em->flush();
        }

        return $this->redirectToRoute('field_index');
    }

    /**
     * Creates a form to delete a field entity.
     *
     * @param Field $field The field entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Field $field)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('field_delete', array('id' => $field->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/delete_field/{id}", name="delete_field")
     * @param Field $field
     * @return JsonResponse
     */
    public function deleteFieldAction(Field $field = null)
    {
        $result = ['status' => 0];
        if ($field) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($field);
            $em->flush();
            $result = ['status' => 1];
        }
        return new JsonResponse($result);
    }
}
