<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nombre'
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Correo electronico',
                'required' => false
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'first_options' => array('label' => 'Contraseña'),
                'second_options' => array('label' => 'Confirmar contraseña')
            ))
            ->add('username', TextType::class, array(
                'label' => 'Username',
                'required' => false
            ))
            ->add('branchOffice', ChoiceType::class, array(
                'label' => 'Sucursal',
                'choices' => array(
                    'Atotonilco' => 1,
                    'Ayotlan' => 2
                )
            ))
            ->add('type', ChoiceType::class, array(
                'label' => 'Permisos',
                'choices' => array(
                    'Administrador' => 0,
                    'Laboratorista' => 1,
                    'Doctor' => 2,
                    'Recepcionista' => 3
                )
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
