<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class ExamType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];

        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'Nombre',
                )
            )
            ->add(
                'tag',
                TextType::class,
                array(
                    'label' => 'Etiqueta',
                )
            )
            ->add(
                'type',
                EntityType::class,
                array(
                    'label' => 'Tipo',
                    'class' => 'AppBundle:Type',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'category',
                EntityType::class,
                array(
                    'label' => 'Categoria',
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'placeholder' => ' ',
                    'required' => false
                )
            )
            ->add(
                'cost',
                MoneyType::class,
                array(
                    'label' => 'Costo',
                    'currency' => 'MXN'
                )
            )
            ->add(
                'fields',
                EntityType::class,
                array(
                    'label' => 'Campos',
                    'class' => 'AppBundle:Field',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($user) {
                        $qb = $er->createQueryBuilder('e');

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                )
            )

//            ->add('fields',
//                EntityType::class,
//                array(
//                    'label' => 'Campos',
//                    'class' => 'AppBundle:Field',
//                    'choice_label' => 'name',
//                    'expanded' => true,
//                    'multiple' => true
//                )
//            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Exam',
                'user' => null
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_exam';
    }


}
