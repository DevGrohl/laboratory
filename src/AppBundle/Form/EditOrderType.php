<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use AppBundle\Entity\Discount;
use AppBundle\Entity\Company;
use AppBundle\Entity\Status;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EditOrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $builder
            ->add('total', MoneyType::class, array(
                'currency' => 'MXN',
                'label' => 'Total'
            ))
            ->add('retainer', MoneyType::class, array(
                'currency' => 'MXN',
                'label' => 'Adelanto'
            ))
            ->add('patient', PatientType::class)
            ->add('doctor', EntityType::class, array(
                'label' => 'Doctor',
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.type = 2')
                        ->orderBy('u.name', 'ASC');
                }
            ))
            ->add('company', EntityType::class, array(
                'label' => 'Empresa',
                'required' => false,
                'class' => Company::class
            ))
            ->add('discount', EntityType::class, array(
                'label' => 'Descuento',
                'required' => false,
                'class' => Discount::class
            ))
            ->add('status', EntityType::class, array(
                'label' => 'Estado',
                'class' => Status::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.id', 'ASC');
                }
            ))
            ->add(
                'created_at',
                DateTimeType::class,
                array(
                    'label' => 'Fecha',
                    'widget' => 'single_text',
                    'format'=> 'yyyy-MM-dd',
                    'attr' => ['class' => 'datepicker'],
                )
            )
            ->add(
                'laboratory_1',
                EntityType::class,
                array(
                    'mapped' => false,
                    'label' => 'Laboratorio 1',
                    'class' => 'AppBundle:Exam',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($user) {
                        $categories = [1,2,3,4,5]; // IDs that are NOT part of the category "lab 1"
                        $qb = $er->createQueryBuilder('e')
                            ->where('e.type = :type')
                            ->andWhere('e.category NOT IN (:categories)')
                            ->setParameter('type', '1')
                            ->setParameter('categories', $categories);

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                )
            )
            ->add(
                'laboratory_2',
                EntityType::class,
                array(
                    'mapped' => false,
                    'label' => 'Laboratorio 2',
                    'class' => 'AppBundle:Exam',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($user) {
                        $qb = $er->createQueryBuilder('e')
                            ->where('e.type = :type')
                            ->setParameter('type', '2');

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                )
            )
            ->add(
                'xray',
                EntityType::class,
                array(
                    'mapped' => false,
                    'label' => 'Radiografias',
                    'class' => 'AppBundle:Exam',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($user){
                        $qb = $er->createQueryBuilder('e')
                            ->join('e.category', 'c')
                            ->where('c.name = :cat')
                            ->setParameter('cat', 'Radiografias');

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                )
            )
            ->add(
                'echo',
                EntityType::class,
                array(
                    'mapped' => false,
                    'label' => 'Ecosonogramas',
                    'class' => 'AppBundle:Exam',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use($user) {
                        $qb = $er->createQueryBuilder('e')
                            ->join('e.category', 'c')
                            ->where('c.name = :cat')
                            ->setParameter('cat', 'Ecosonogramas');

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                )
            )
            ->add(
                'contrasted',
                EntityType::class,
                array(
                    'mapped' => false,
                    'label' => 'Contrastados',
                    'class' => 'AppBundle:Exam',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($user){
                        $qb = $er->createQueryBuilder('e')
                            ->join('e.category', 'c')
                            ->where('c.name = :cat')
                            ->setParameter('cat', 'Contrastados');

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                )
            )
            ->add(
                'mama',
                EntityType::class,
                array(
                    'mapped' => false,
                    'label' => 'Mama',
                    'class' => 'AppBundle:Exam',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($user) {
                        $qb = $er->createQueryBuilder('e')
                            ->join('e.category', 'c')
                            ->where('c.name = :cat')
                            ->setParameter('cat', 'Mama');

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                )
            )
            ->add(
                'scan',
                EntityType::class,
                array(
                    'mapped' => false,
                    'label' => 'Tomografia',
                    'class' => 'AppBundle:Exam',
                    'choice_label' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use($user) {
                        $qb = $er->createQueryBuilder('e')
                            ->join('e.category', 'c')
                            ->where('c.name = :cat')
                            ->setParameter('cat', 'Tomografia');

                        if ($user->getBranchOffice() !== 0){
                            $qb->innerJoin('AppBundle:User', 'u', 'WITH', 'u.branch_office = :branch');
                            $qb->andWhere('u.id = e.createdBy');
                            $qb->setParameter('branch', $user->getBranchOffice());
                        }

                        return $qb;
                    },
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Order',
            'user' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_order';
    }


}
