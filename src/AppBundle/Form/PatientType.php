<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class PatientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nombre'
                ))
            ->add('age', NumberType::class, array(
                'label' => 'Edad',
                'required' => false
            ))
            ->add('birthdate', BirthdayType::class, array(
                'label' => 'Fecha de nacimiento',
                'required' => false
            ))
            ->add('address', TextType::class, array(
                'label' => 'Dirección',
                'required' => false
            ))
            ->add('phone', NumberType::class, array(
                'label' => 'Número de teléfono',
                'required' => false
            ))
            ->add('cellphone', NumberType::class, array(
                'label' => 'Número celular',
                'required' => false
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'required' => false
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Patient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_patient';
    }


}
