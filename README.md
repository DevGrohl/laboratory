Labs project
========================

# How to use this repo
--------------

## Requirements

 * PHP
 * MySQL
 * Composer
 
## Clone the repo

```
git clone https://gitlab.com/DevGrohl/laboratory
```

## Install dependencies

```bash
composer install
```

## Running the Symfony server

```bash
php bin/console server:run
```